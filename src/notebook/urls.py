from django.urls import path
from notebook.views import (
    create_blog_view,
    detail_blog_post,

)

app_name = 'notebook'

urlpatterns = [
    path('create/', create_blog_view, name='create'),
    path('detail/', detail_blog_post, name='detail'), #slug !!!!! pokombinowac ze slugiem bo inaczej nie wywietli więcej niż dwa posty

]