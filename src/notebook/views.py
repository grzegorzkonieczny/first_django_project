from django.shortcuts import render, redirect, get_object_or_404
from notebook.models import Post
from notebook.forms import CreatePostForm
from account.models import Account
from notebook.models import Post


# def create_blog_view(request):
#     context = {}
#     user = request.user
#     if not user.is_authenticated:
#         return redirect('must_authenticated')
#     form = CreatePostForm(request.POST)
#     if form.is_valid():
#         form.save()
#         author = Account.objects.filter(email=user.email).first()
#         author.save()
#         form = CreatePostForm()
#     context['form'] = form
#     return render(request, 'notebook/create_blog.html', context)

def home_screen_view(request):
    context = {}
    blog_posts = Post.objects.all()
    context['blog_posts'] = blog_posts
    return render(request, 'notebook/home.html', context)


def create_blog_view(request):
    context = {}
    user = request.user
    if not user.is_authenticated:
        return redirect('must_authenticated')
    form = CreatePostForm(request.POST)
    if form.is_valid():
        obj = form.save(commit=False)
        author = Account.objects.filter(email=user.email).first()
        obj.author = author
        obj.save()
        form = CreatePostForm()
    context['form'] = form
    return render(request, 'notebook/create_blog.html', context)


def detail_blog_post(request):
    context = {}
    blog_post = get_object_or_404(Post)
    context['blog_post'] = blog_post
    return render(request, 'notebook/detail_blog.html', context)