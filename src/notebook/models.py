from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.


class Post(models.Model):
    STATUS_CHOICES = (
        ('draft','draft'),
        ('published','published')
    )
    author = models.ForeignKey('account.Account', on_delete=models.CASCADE, related_name='blog_post')
    title = models.CharField(max_length=250)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')
    slug  = models.SlugField(blank=True, unique_for_date=publish) #slug to url, unique_for_date=publish-bud adresów wg daty publikacji

    class Meta:
        ordering = ('-publish',)
        verbose_name = 'Nowy post'
        verbose_name_plural = 'Posty'

    def __str__(self):
        return self.title


