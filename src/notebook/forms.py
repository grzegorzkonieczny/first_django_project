from django import forms
from notebook.models import Post


class CreatePostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['title', 'body']


