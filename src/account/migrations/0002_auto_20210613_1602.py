# Generated by Django 3.2.3 on 2021-06-13 14:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='account',
            options={'verbose_name': 'konto', 'verbose_name_plural': 'Konta użytkowników'},
        ),
        migrations.RemoveField(
            model_name='account',
            name='first_name',
        ),
    ]
