from django.contrib import admin
from .models import Account
from django.contrib.auth.admin import UserAdmin
# Register your models here.

#admin.site.register(Account)
@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('email', 'username', 'date_joined',
                    'last_login', 'is_admin', 'is_active', 'is_staff', 'is_superuser')
    list_filter = ('username', 'is_active', 'date_joined')
    readonly_fields = ('date_joined','last_login')
    search_fields = ('username', 'first_name', 'email')

    # filter_horizontal = ()
    # fieldsets = ()
#admin.site.register(Account,AccountAdmin)