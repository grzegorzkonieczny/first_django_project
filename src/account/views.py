from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from account.forms import RegistrationForm, AccountAuthenticationForm, AccountUpdateForm
from account.models import Account
from notebook.models import Post

# Create your views here.

# def home_screen_view(request): # ten widok trzeba przenieść do notebook.wiev
#     context = {}
#     blog_post = Post.objects.all()
#     context['blog_post'] = blog_post
#     return render(request, 'notebook/home.html', context)


def registration_view(request):
    context = {}
    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            account = authenticate(email=email, password=raw_password)
            login(request, account)
            return redirect('home')
        else:
            context['registration_form'] = form
    else:
        form = RegistrationForm()
        context['registration_form'] = form

    return render(request, 'account/register.html', context)

def logout_view(request):
    logout(request)
    return redirect('home')

def login_view(request):
    context = {}
    user = request.user
    if user.is_authenticated: #jeżli nazwa użytkownia jest niepowtarzalna?, prawdziwa?
        return redirect('home')
    if request.POST: #login.form
        form = AccountAuthenticationForm(request.POST) # informajca, że forma dot AccountAuthenticationForm
        if form.is_valid(): #sprawdzamy czy forma jest prawdziwa
           email = request.POST['email']
           password = request.POST['password']
           user = authenticate(email=email, password=password)

           if user: #sprawdzamy czy email i hasło są poprawne
               login(request, user)
               return redirect('home')
    else:
        form = AccountAuthenticationForm()
    context['login_form'] = form
    return render(request, 'account/login.html', context)

def account_view(request):
    if not request.user.is_authenticated:
        return redirect('login')
    context = {}
    if request.POST:
        form = AccountUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
    else:
        form = AccountUpdateForm(
            initial = {
                'email': request.user.email,
                'userame': request.user.username,
            }
        )
    context['account_form'] = form

    # widok do listy postów
    blog_posts = Post.objects.filter(author=request.user)
    context['blog_posts'] = blog_posts

    return render(request, 'account/account.html', context)


#must authenticated,
def must_authenticated_view(request):
    return render(request, 'account/must_authenticated.html', {})
